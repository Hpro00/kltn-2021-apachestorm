# KLTN-2021-ApacheStorm

Deploy Apache Storm cluster include Nimbus, Supervisor, Zookeeper
And Mysql for store data + MQTT broker.

## Getting started

Create a Jenkinsfile. Add Stage and Build.
Jenkins must be config with SonarQube
yyy
## Expand

If you write Storm Topology with multiple worker. Please Use storm-worker.yaml file with contain 4 worker and a headless service by default.

## Check Result In 2 ways: CICD and Offline

If you use CICD to build => After data published. Use SSH Tunnel to port 9999

If you want to get data offline, just open port 9999, everything there!
