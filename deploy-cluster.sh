#!/bin/sh
echo "Deploying MQTT Broker..."

kubectl apply -f MQTT/.

echo "Done! MQTT has been deployed successfully"

echo "Deploying Mysql..."

kubectl apply -f Mysql/.

echo "Done! Mysql has been deployed successfully"

echo "Deploying Cluster..."

kubectl apply -f Storm/zookeeper.yaml
echo "Done! Zookeeper has been deployed successfully"

sleep 4

kubectl apply -f Storm/nimbus.yaml
echo "Done! Nimbus has been deployed successfully" 

sleep 20

kubectl apply -f Storm/ui.yaml
kubectl apply -f Storm/storm-alone.yaml
echo 'Done! Ui and Supervisor has been deployed successfully' 

echo "Everything Deploy SuccessFul."


