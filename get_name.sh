VERSION=$(cat pom.xml | grep version | awk 'NR==2' | sed 's/<[^>]*>//g'  | tr -d "[:blank:]")
ARTIFACTID=$(cat pom.xml | grep artifactId | awk 'NR==1' | sed 's/<[^>]*>//g' | tr -d "[:blank:]")

export JAR_NAME="$ARTIFACTID-$VERSION-jar-with-dependencies.jar"
echo $JAR_NAME

