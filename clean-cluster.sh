#!/bin/sh

echo "Deleting Storm Components..."
kubectl delete -f Storm/.

echo "Deleting MQTT Broker..."
kubectl delete -f MQTT/.

echo "Deleting MySQL..."
timeout 2 kubectl delete -f Mysql/.
kubectl delete pvc $(kubectl get pvc | grep mysql | awk '{print $1}')
echo "Deleted!"
